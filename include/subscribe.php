<?php

require_once 'MCAPI.class.php';

/*
$apiKey = ''; // Your MailChimp API Key
$listId = ''; // Your MailChimp List ID
$double_optin=false;
$send_welcome=false;
$email_type = 'html';
$email = $_POST['widget-subscribe-form-email'];
$datacenter = explode( '-', $apiKey );
$submit_url = "http://" . $datacenter[1] . ".api.mailchimp.com/1.3/?method=listSubscribe";
*/

$email = $_POST['widget-subscribe-form-email'];

if( isset( $email ) AND $email != '' ) {

    /*
    $data = array(
        'email_address'=>$email,
        'apikey'=>$apiKey,
        'id' => $listId,
        'double_optin' => $double_optin,
        'send_welcome' => $send_welcome,
        'email_type' => $email_type
    );

    $payload = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $submit_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, urlencode($payload));

    $result = curl_exec($ch);
    curl_close ($ch);
    $data = json_decode($result);

    if ( isset( $data->error ) AND $data->error != '' ){
        echo $data->error;
    } else {
        echo 'You have been <strong>successfully</strong> subscribed to our Email List.';
    }
    */

    // grab an API Key from http://admin.mailchimp.com/account/api/
    $api = new MCAPI('cd03a5f07a44403435f7f30785f6cb47-us1');
    // grab your List's Unique Id by going to http://admin.mailchimp.com/lists/
    // Click the "settings" link for the list - the Unique Id is at the bottom of that page.
    $list_id = "89e5fcaad2";

    if (!empty($_GET["ref"])) {
        $merge_vars = array(
            'SOURCE' => $_GET["ref"]
        );
    } else {
        $merge_vars = array(
            'SOURCE' => 'LandingPage'
        );
    }

    $params = array();
    $params["id"] = $list_id;
    $params["email_address"] = $email;
    $params["merge_vars"] = $merge_vars;
    $params["email_type"] = 'html';
    $params["double_optin"] = false;
    $params["update_existing"] = false;
    $params["replace_interests"] = true;
    $params["send_welcome"] = false;

    //if($api->listSubscribe($list_id, $_POST['email'], '', 'html',false,'','',false) === true) {
    if($api->callServer("listSubscribe", $params) === true) {
        // It worked!
        //return 'Success!';
        //echo "<script>ga('send', 'event', 'email', 'newSubscription');</script>";
        //echo "<script>ga('send', 'event', 'email', 'newSubscription', 'webSite', 1);</script>";
        //echo "<script>swal('test', 'success');</script>";
    }else{
        // An error ocurred, return error message
        //return 'Error: ' . $api->errorMessage;
    }

    echo "<script>swal('"._('Au top !')."', '"._('Un grand merci de la part de Pistache. \\n\\nNous reviendrons vers vous rapidement !')."', 'success');</script>";


}

?>