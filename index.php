<!DOCTYPE html>
<html dir="ltr" lang="fr-FR">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="AppiNest" content="DreamZ" />

	<meta name="description" content="Dreamzgame, une application pour créer le jeu vidéo de votre imagination simplement & en apprenant les bases de la programmation informatique">
	<meta name="keywords" content="Dreamzgame construction apprendre coder éducation jeu">

	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@DreamzGame">
	<meta name="twitter:creator" content="@DreamzGame">
	<meta name="twitter:title" content="Dreamz : l'apprentissage de la programmation par le jeu">
	<meta name="twitter:description" content="Dreamzgame, une application pour créer le jeu vidéo de votre imagination simplement & en apprenant les bases de la programmation informatique">
	<meta name="twitter:image" content="http://www.appinest.fr/dreamz/img/social.jpg">

	<meta property="og:title" content="Dreamz : l'apprentissage de la programmation par le jeu" />
	<meta property="og:image" content="http://www.appinest.fr/dreamz/img/social.jpg" />
	<meta property="og:description" content="Dreamzgame, une application pour créer le jeu vidéo de votre imagination simplement & en apprenant les bases de la programmation informatique" />
	<meta property="og:url" content="http://www.appinest.fr/dreamz"/>
	<meta property="og:type" content="article" />
	<meta property="og:site_name" content="Dreamz : l'apprentissage de la programmation par le jeu" />


    <link rel="icon" type="image/png" href="img/favicon.ico">

    <!-- Stylesheets
    ============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
    <link href="css/sweetalert.css" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <!--[if lt IE 9]>
    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <!-- External JavaScripts
    ============================================= -->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/plugins.js"></script>
    <script src="js/sweetalert-dev.js"></script>

    <!-- Document Title
    ============================================= -->
    <title>DreamZ - Apprendre  à coder</title>

</head>

<body class="stretched">


<?php
if (!empty($_POST['formSubmit'])) {

    $name = $_POST['formName'];
    $email = $_POST['formEmail'];
    $message = $_POST['formMessage'];
    $subject = $_POST['formSubject'];
    $from = 'From: '.$email;
    $to = 'thibault@pistache-app.com';
    $subject = 'New message from appinest.fr by '.$name;
    //$human = $_POST['human'];

    $body = "Email : $email\nName : $name\n\nSubject : $subject \nMessage :\n $message";

    if (mail ($to, $subject, $body, $from)) {
        //echo '<p>Your message has been sent!</p>';
        //echo "<script>swal('Info', 'message envoyé, merci !');</script>";
        echo "<script>swal('"._('Merci !')."', '"._('Merci pour votre message, nous vous répondrons rapidement !')."', 'success');</script>";
    } else {
        //echo '<p>Something went wrong, go back and try again!</p>';
        echo "<script>swal('Info', '"._('Une erreur est arrivée')."');</script>";
    }
}
?>


    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <div id="home" class="page-section" style="position:absolute;top:0;left:0;width:100%;height:200px;z-index:-2;"></div>

        <section id="slider" class="slider-parallax full-screen with-header swiper_wrapper clearfix">

            <div class="swiper-container swiper-parent">
                <div class="swiper-wrapper"> <!--full-screen force-full-screen dark section nopadding nomargin noborder ohidden -->
                    <div class="swiper-slide dark" style="background-image: url('img/Soon_empty.jpg');">
                        <div class="container clearfix" style="width: 100%"> <!-- col-md-6 dark fright nobottommargin fadeIn animated -->
                            <div class="col-md-4 dark fright nobottommargin fadeIn animated" style="width:100%">
                                <div data-caption-animate="fadeInUp" class="slider-caption slider-caption-center">
                                    <h2 style="text-shadow: 0px 0px 12px #030303">Dreamz</h2>
                                    <p  data-caption-delay="200" style="text-shadow: 0px 0px 6px #030303">Build and Share<br>Your imagination will be your limit</p>

                                    <div style="padding-top: 30px">
                        				<a href="   https://s3-eu-west-1.amazonaws.com/dreamzdata/Build/Dreamz_Mac.zip" class="button button button-rounded uppercase" target="_blank">
                                            <div>Mac (beta)</div>
                                        </a>

                                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>

                                        <a href="   https://s3-eu-west-1.amazonaws.com/dreamzdata/Build/Dreamz_Windows.zip" class="button button button-rounded uppercase nomargin" target="_blank"><div>Windows (beta)</div></a>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide dark" style="background-image: url('img/social.jpg');">
                        <h2 style="text-shadow: 0px 0px 12px #030303;">Build your own levels</h2>
                    </div>
                    <div class="swiper-slide dark" style="background-image: url('img/adventure.jpg');">
                        <h2 style="text-shadow: 0px 0px 12px #030303">Enjoy the Adventure</h2>
                    </div>
                    <div class="swiper-slide dark" style="background-image: url('img/level1.jpg');">
                        <h2 style="text-shadow: 0px 0px 12px #030303">Finish our amazing levels</h2>
                    </div>
                    <div class="swiper-slide dark" style="background-image: url('img/playothers.jpg');">
                        <h2 style="text-shadow: 0px 0px 12px #030303">Try other players's levels</h2>
                    </div>
                </div>

                <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
                <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
                <div id="slide-number"><div id="slide-number-current"></div><span>/</span><div id="slide-number-total"></div></div>

            </div>

            <script>
                jQuery(document).ready(function($){
                    var swiperSlider = new Swiper('.swiper-parent',{
                        paginationClickable: false,
                        slidesPerView: 1,
                        grabCursor: true,
                        onSwiperCreated: function(swiper){
                            $('[data-caption-animate]').each(function(){
                                var $toAnimateElement = $(this);
                                var toAnimateDelay = $(this).attr('data-caption-delay');
                                var toAnimateDelayTime = 0;
                                if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ) + 750; } else { toAnimateDelayTime = 750; }
                                if( !$toAnimateElement.hasClass('animated') ) {
                                    $toAnimateElement.addClass('not-animated');
                                    var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                                    setTimeout(function() {
                                        $toAnimateElement.removeClass('not-animated').addClass( elementAnimation + ' animated');
                                    }, toAnimateDelayTime);
                                }
                            });
                        },
                        onSlideChangeStart: function(swiper){
                            $('#slide-number-current').html(swiper.activeIndex + 1);
                            $('[data-caption-animate]').each(function(){
                                var $toAnimateElement = $(this);
                                var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                                $toAnimateElement.removeClass('animated').removeClass(elementAnimation).addClass('not-animated');
                            });
                        },
                        onSlideChangeEnd: function(swiper){
                            $('#slider .swiper-slide').each(function(){
                                if($(this).find('video').length > 0) { $(this).find('video').get(0).pause(); }
                            });
                            $('#slider .swiper-slide:not(".swiper-slide-active")').each(function(){
                                if($(this).find('video').length > 0) {
                                    if($(this).find('video').get(0).currentTime != 0 ) $(this).find('video').get(0).currentTime = 0;
                                }
                            });
                            if( $('#slider .swiper-slide.swiper-slide-active').find('video').length > 0 ) { $('#slider .swiper-slide.swiper-slide-active').find('video').get(0).play(); }

                            $('#slider .swiper-slide.swiper-slide-active [data-caption-animate]').each(function(){
                                var $toAnimateElement = $(this);
                                var toAnimateDelay = $(this).attr('data-caption-delay');
                                var toAnimateDelayTime = 0;sendemail
                                if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ) + 300; } else { toAnimateDelayTime = 300; }
                                if( !$toAnimateElement.hasClass('animated') ) {
                                    $toAnimateElement.addClass('not-animated');
                                    var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                                    setTimeout(function() {
                                        $toAnimateElement.removeClass('not-animated').addClass( elementAnimation + ' animated');
                                    }, toAnimateDelayTime);
                                }
                            });
                        }
                    });

                    $('#slider-arrow-left').on('click', function(e){
                        e.preventDefault();
                        swiperSlider.swipePrev();
                    });

                    $('#slider-arrow-right').on('click', function(e){
                        e.preventDefault();
                        swiperSlider.swipeNext();
                    });

                    $('#slide-number-current').html(swiperSlider.activeIndex + 1);
                    $('#slide-number-total').html(swiperSlider.slides.length);
                });
            </script>

        </section>

        <!-- Header
        ============================================= -->
        <header id="header" class="full-header">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png">         <!-- 404 page not found, replaced .html by .php !-->
                            <img style="padding:10px" src="img/logo_short2.png" alt="Canvas Logo"></a>
                        <a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png">       <!-- 404 page not found !  replaced .html by .php-->
                            <img style="padding:10px" src="img/logo_short2.png" alt="Canvas Logo"></a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <ul class="one-page-menu">
                            <li><a href="#" data-href="#content"><div>Concept</div></a></li>
                            <li><a href="http://dreamzgame.appinest.fr" data-href="#section-pricing"><div>Try Dreamz</div></a></li>
                            <li><a href="#" data-href="#section-contact"><div>Contact us</div></a></li>
                        </ul>

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->

        <!-- Content
        ============================================= -->
        <section id="content" class="responsive-left">

            <div class="content-wrap" style="padding-bottom: 0px">

                <div class="container clearfix">

                    <div class="divcenter center" style="padding-bottom:6em">
                        <h2> Create levels and challenge your friends</h2>

                        <h4>What if you had everything you need to create awesome level? <br>
                        Dreamz is the game where you can create thousand levels based on your imagination .From amazing maze to very specific IA trough visual programming, try to surprise your friends. <br><br>

                        Today, Dreamz is in Beta. <br>
                        Feel free to try the game, but then, we would love to hear advices from you. <br>
                        So feel free to send us an email :) <br><br><br>
                        <b>Download the beta!</b></h4>

                        <div style="padding-top:0em">
	                        <!--<a href="#section-contact" data-href="#section-contact" class="button button-border button button-rounded uppercase"><div>Contact us</div></a>-->

                            <!--for both the following buttons, removed the nomargin class so that when they are on top of another they don't touch.
                            also removed the span since it is not necesseary anymore.
                            -->
                            <a href="   https://s3-eu-west-1.amazonaws.com/dreamzdata/Build/Dreamz_Mac.zip" class="button button button-rounded uppercase" target="_blank"><div>Mac (beta)</div></a>


                            <a href="   https://s3-eu-west-1.amazonaws.com/dreamzdata/Build/Dreamz_Windows.zip" class="button button button-rounded uppercase" target="_blank"><div>Windows (beta)</div></a>

                        </div>

                    </div>

                    <div class="clear"></div>

                </div>

            </div>


        </section><!-- #content end -->


        <section id="section-contact" class="page-section full-screen section nopadding nomargin noborder ohidden responsive-right" >
            <div class="vertical-middle">
                <div class="container clearfix" id = "small-container">

                    <!-- Contact Form Overlay
                    ============================================= -->
                    <div id="contact-form-overlay-mini" class="clearfix">

                        <div class="fancy-title title-dotted-border">
                            <h3>Let's talk!</h3>
                        </div>

                        <!-- Contact Form
                        ============================================= -->
                        <div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>

                        <form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

                        	<!--
                            <div class="col_full">
                                <label for="template-contactform-name">Name / Surname <small>*</small></label>
                                <input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
                            </div>
                            -->

                            <div class="col_full">
                                <label for="template-contactform-email">Email <small>*</small></label>
                                <input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
                            </div>

                            <div class="clear"></div>

                            <!--
                            <div class="col_full">
                                <label for="template-contactform-subject">Subject <small>*</small></label>
                                <input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" />
                            </div>
                            -->

                            <div class="col_full">
                                <label for="template-contactform-message">Message <small>*</small></label>
                                <textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
                            </div>

                            <div class="col_full hidden">
                                <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
                            </div>

                            <div class="col_full">
                                <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send</button>
                            </div>

                        </form>

                        <script type="text/javascript">
                            $("#template-contactform").validate({
                                submitHandler: function(form) {
                                    $('.form-process').fadeIn();
                                    $(form).ajaxSubmit({
                                        target: '#contact-form-result',
                                        success: function() {
                                            $('.form-process').fadeOut();
                                            $('#template-contactform').find('.sm-form-control').val('');
                                            $('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
                                            SEMICOLON.widget.notifications($('#contact-form-result'));
                                        }
                                    });
                                }
                            });
                        </script>

                    </div><!-- Contact Form Overlay End -->

                </div>
            </div>

            <!-- Google Map    ??? nothing to do with google maps ???
            ============================================= -->
<!--            <section id="google-map" class="gmap full-screen">
                    <img style="width: 100%" src="img/Soon_empty.jpg"></a>
                    why was this a thing anyway??
            </section>
-->

        </section>


        <!-- Footer
        ============================================= -->
        <footer id="footer" class="dark">

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_full">
                        Copyrights &copy; 2017 All rights reserved<br>
                        <!--<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>-->
                    </div>

                    <div class="col_half col_last tright">
                        <div class="clear"></div>

                        <!--<i class="icon-envelope2"></i> thibault@appinest.fr-->
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- Footer Scripts
    ============================================= -->
    <script type="text/javascript" src="js/functions.js"></script>

</body>
</html>
